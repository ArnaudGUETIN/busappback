package com.bonapp.app.service;

import com.bonapp.app.entities.Commande;
import com.bonapp.app.entities.Menu;
import com.bonapp.app.modelView.MenuListMV;
import com.bonapp.app.modelView.MenuMV;
import com.bonapp.app.modelView.PlatMV;

import java.util.List;

public interface MenuService {

    List<Menu> getAllMenus();
    List<MenuMV> getAllMenusMV();
    List<MenuMV> getAllMenusMV(List<Menu> menus);
    MenuMV getOneMenuMV(Menu menu);
    MenuMV getOneMenuMVById(Long menuId);
    List<MenuMV> getPreviousMenus(Long clientId);
    List<Menu> getCommandesMenus(List<Commande> commandes);
    List<MenuMV> getRestaurantMenus(Long restaurantId);
    List<MenuListMV>  getMenuListMV(List<MenuMV> menuMVS,Long restaurantId);
    String generateComposition(List<PlatMV> platMVS);
    List<MenuMV> sortMenuMvByCategorieName(List<MenuMV> menuMVS);

    Menu addMenu(Menu menu);
    boolean deleteMenu(Long menuId);
    Menu updateMenu(Long menuId,Menu menu);
}
