package com.bonapp.app.service;

import com.bonapp.app.entities.Client;
import com.bonapp.app.modelView.ClientMV;

import java.util.List;

public interface ClientService {
    List<Client> getAllClients();
    List<ClientMV> getAllClientsMV();
    List<ClientMV> getAllClientsMV(List<Client> clients);
    Client addClient(ClientMV clientMV);
    ClientMV getOneClientMV(Client client);
    ClientMV getOneClientMVById(Long clientId);
    Client getOneClientById(Long clientId);
    Client addClient(Client client);
    boolean deleteClient(Long clientId);
    Client updateClient(Long clientId,ClientMV clientMV);
}
