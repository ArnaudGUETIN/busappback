package com.bonapp.app.service;

import com.bonapp.app.dao.BoissonRepository;
import com.bonapp.app.entities.Boisson;
import com.bonapp.app.modelView.BoissonMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class BoissonServiceImpl implements BoissonService {

    @Autowired
    private BoissonRepository boissonRepository;

    @Override
    public List<BoissonMV> getAllBoissonsMvs() {
        List<Boisson> boissonList = boissonRepository.findAll();
        List<BoissonMV> boissonMVList = new ArrayList<>();
        for(Boisson boisson:boissonList){
            BoissonMV platMV = new BoissonMV(boisson);
            boissonMVList.add(platMV);
        }

        return boissonMVList;
    }

    @Override
    public Set<BoissonMV> getPlatsOfCategorie(Long categorieId) {
        return null;
    }

    @Override
    public Set<BoissonMV> getBoissonsOfCategorieMV(Long categorieId) {
        return null;
    }

    @Override
    public BoissonMV getOneBoissonMV(Boisson boisson) {
        return new BoissonMV(boisson);
    }

    @Override
    public BoissonMV getOneBoissonMVById(Long boissonId) {
        Boisson boisson = boissonRepository.getOne(boissonId);
        if(boisson!=null){
            return getOneBoissonMV(boisson);
        }
        return null;
    }

    @Override
    public List<BoissonMV> getBoissonMvs(List<Boisson> boissons) {
        Set<BoissonMV> toReturn = new HashSet<>();
        for(Boisson boisson:boissons){
            BoissonMV boissonMV = getOneBoissonMV(boisson);
            toReturn.add(boissonMV);
        }
        return new ArrayList<>(toReturn);
    }

    @Override
    public Boisson addBoisson(Boisson boisson) {
        return null;
    }

    @Override
    public boolean deleteBoisson(Long boissonId) {
        return false;
    }

    @Override
    public Boisson updateBoisson(Long boissonId, Boisson boisson) {
        return null;
    }
}
