package com.bonapp.app.service;

import com.bonapp.app.entities.Supplement;
import com.bonapp.app.modelView.SupplementMV;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class SupplementServiceImpl implements SupplementService {
    @Override
    public List<SupplementMV> getAllSupplementsMvs() {
        return null;
    }

    @Override
    public Set<SupplementMV> getPlatsOfCategorie(Long categorieId) {
        return null;
    }

    @Override
    public Set<SupplementMV> getSupplementsOfCategorieMV(Long categorieId) {
        return null;
    }

    @Override
    public SupplementMV getOneSupplementMV(Supplement supplement) {
        return null;
    }

    @Override
    public SupplementMV getOneSupplementMVById(Long supplementId) {
        return null;
    }

    @Override
    public List<SupplementMV> getSupplementMvs(List<Supplement> supplements) {
        return null;
    }

    @Override
    public Supplement addSupplement(Supplement supplement) {
        return null;
    }

    @Override
    public boolean deleteSupplement(Long supplementId) {
        return false;
    }

    @Override
    public Supplement updateSupplement(Long supplementId, Supplement supplement) {
        return null;
    }
}
