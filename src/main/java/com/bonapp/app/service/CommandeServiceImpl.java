package com.bonapp.app.service;

import com.bonapp.app.dao.ClientRepository;
import com.bonapp.app.dao.CommandeRepository;
import com.bonapp.app.dao.LivreurRepository;
import com.bonapp.app.dao.MenuRepository;
import com.bonapp.app.entities.Commande;
import com.bonapp.app.entities.LigneCommande;
import com.bonapp.app.entities.Livraison;
import com.bonapp.app.entities.Livreur;
import com.bonapp.app.modelView.CommandeMV;
import com.bonapp.app.modelView.LigneCommandeMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ZERO;

@Service
@Transactional
public class CommandeServiceImpl implements CommandeService {

    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private LigneCommandeService ligneCommandeService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private LivreurRepository livreurRepository;
    @Autowired
    private LivraisonService livraisonService;
//    @Autowired
//    SimpMessageSendingOperations messagingTemplate;


    @Override
    public List<Commande> getAllCommandes() {
        return commandeRepository.findAll();
    }

    @Override
    public List<CommandeMV> getAllCommandesMV() {
        return getAllCommandesMV(getAllCommandes());
    }

    @Override
    public List<CommandeMV> getAllCommandesMV(List<Commande> commandes) {
        List<CommandeMV> toReturn = new ArrayList<>();
        for(Commande commande:commandes){
            CommandeMV commandeMV = new CommandeMV(commande);
            if(!CollectionUtils.isEmpty(commande.getLigneCommandes())){
                commandeMV.setLigneCommandeMVS(ligneCommandeService.getAllLigneCommandesMV(new ArrayList<>(commande.getLigneCommandes())));
                commandeMV.setTotalCommande(getTotalCommande(new ArrayList<>(commande.getLigneCommandes())));
            }

            toReturn.add(commandeMV);
        }
        return toReturn;
    }

    @Override
    public CommandeMV getOneCommandeMV(Commande commande) {
        return new CommandeMV(commande);
    }

    @Override
    public CommandeMV getOneCommandeMVById(Long commandeId) {
        Commande commande = commandeRepository.getOne(commandeId);
        if(commande!=null){
            return getOneCommandeMV(commande);
        }
        return null;
    }

    @Override
    public List<CommandeMV> getPreviousCommande(Long restaurantId, Long clienId) {
        List<Commande> commandeList = new ArrayList<>();
        for(Commande commande:clientRepository.getOne(clienId).getCommandes()){
            if(commande.getRestaurant().getRestaurantId().equals(restaurantId)){
                commandeList.add(commande);
            }

        }
        return getAllCommandesMV(commandeList);
    }



    @Override
    public Commande addCommande(CommandeMV commandeMV) {
        Commande commande = new Commande();
        commande.setDateCommande(new Date());
        if(commandeMV.getDescription().isEmpty()){
            for(Long menuId:commandeMV.getMenusIds()){
                commande.setDescription(commande.getDescription()+"-"+menuRepository.getOne(menuId).getLibelle());
            }
        }else {
            commande.setDescription(commandeMV.getDescription());
        }

        commande.setInstruction(commandeMV.getInstruction());
        commande.setNumeroCommande("#CFBHJ225");
        commande.setAdresseLivraison(commandeMV.getAdresseLivraison());
        commande.setRestaurant(restaurantService.getOneRestaurantById(commandeMV.getRestaurantId()));
        commande.setClient(clientRepository.getOne(commandeMV.getClientId()));
        commande.setStatut(Commande.STATUT_COMMANDE_ENVOYE);
        commande = commandeRepository.save(commande);
        for (LigneCommandeMV ligneCommandeMV :commandeMV.getLigneCommandeMVS()){
            ligneCommandeService.addLigneCommandeToCommande(ligneCommandeService.addLigneCommande(ligneCommandeMV).getLigneCommandeId(),commande.getCommandeId());
        }
        //messagingTemplate.convertAndSend("/topic/progress", commandeMV);
        return commandeRepository.save(commande);
    }

    @Override
    public boolean deleteCommande(Long commandeId) {
        return false;
    }

    @Override
    public Commande updateCommande(Long commandeId, Commande commande) {
        return null;
    }


    public static BigDecimal getTotalCommande(List<LigneCommande> ligneCommandes){
        BigDecimal toReturn = ZERO;
        if(ligneCommandes!=null){
            for (LigneCommande ligneCommande:ligneCommandes){
                toReturn = toReturn.add(ligneCommande.getPrixUnitaire().multiply(BigDecimal.valueOf(ligneCommande.getQuantite())));
            }
        }
        return toReturn;
    }

    @Override
    public Commande nextStatut(Commande commande, int step) {
        commande.setStatut(step);
        commandeRepository.save(commande);
        return commandeRepository.save(commande);
    }

    @Override
    public Commande acceptCommandeByRestaurant(Long commandeId, Date dateAcception) {
        Commande commande = commandeRepository.getOne(commandeId);
        commande.setDateAcceptation(dateAcception);
       // messagingTemplate.convertAndSend("/topic/progress", commande);
        return commandeRepository.save(commande);
    }

    @Override
    public Commande acceptCommandeByLivreur(Long commandeId, Date datePriseEnCharge, Long livreurId) {
        Commande commande = commandeRepository.getOne(commandeId);
        Livreur livreur = livreurRepository.getOne(livreurId);
        Livraison livraison = new Livraison();
        livraison.setClient(commande.getClient());
        livraison.setCommande(commande);
        livraison.setLivreur(livreur);
        livraison.setRestaurant(commande.getRestaurant());
        livraison.setStatut(Livraison.STATUT_LIVRAISON_EN_COURS);
        livraisonService.addLivraison(livraison);
        commande.setDatePriseEncharge(datePriseEnCharge);
      //  messagingTemplate.convertAndSend("/topic/progress", commande);
        return commandeRepository.save(commande);
    }
}
