package com.bonapp.app.service;

import com.bonapp.app.entities.Commande;
import com.bonapp.app.modelView.CommandeMV;

import java.util.Date;
import java.util.List;

public interface CommandeService {

    List<Commande> getAllCommandes();
    List<CommandeMV> getAllCommandesMV();
    List<CommandeMV> getAllCommandesMV(List<Commande> commandes);
    CommandeMV getOneCommandeMV(Commande commande);
    CommandeMV getOneCommandeMVById(Long commandeId);
    List<CommandeMV> getPreviousCommande(Long restaurantId,Long clienId);

    Commande addCommande(CommandeMV commandeMV);
    boolean deleteCommande(Long commandeId);
    Commande updateCommande(Long commandeId,Commande commande);
    Commande nextStatut(Commande commande,int step);
    Commande acceptCommandeByRestaurant(Long commandeId, Date dateAcception);
    Commande acceptCommandeByLivreur(Long commandeId, Date datePriseEnCharge,Long livreurId);

}
