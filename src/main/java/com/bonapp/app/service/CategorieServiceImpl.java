package com.bonapp.app.service;

import com.bonapp.app.dao.CategorieRepository;
import com.bonapp.app.entities.Categorie;
import com.bonapp.app.modelView.CategorieMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class CategorieServiceImpl implements CategorieService {

    @Autowired
    private CategorieRepository categorieRepository;

    @Override
    public Set<Categorie> getAllCategories() {
        return new HashSet<>(categorieRepository.findAll());
    }

    @Override
    public Set<CategorieMV> getAllCategoriesMV() {
        return getAllCategoriesMV(new ArrayList<>(getAllCategories()));
    }

    @Override
    public Set<CategorieMV> getAllCategoriesMV(List<Categorie> categories) {
        Set<CategorieMV> toReturn = new HashSet<>();
        for(Categorie categorie:categories){
            CategorieMV categorieMV = new CategorieMV(categorie.getCategorieId(),categorie.getDesignation(),categorie.getRestaurant().getNom(),categorie.getPhoto());
            toReturn.add(categorieMV);
        }
        return toReturn;
    }

    @Override
    public CategorieMV getOneCategorieMV(Categorie categorie) {
        return new CategorieMV(categorie);
    }

    @Override
    public CategorieMV getOneCategorieMVById(Long categorieId) {
        Categorie categorie = categorieRepository.getOne(categorieId);
        if(categorie!=null){
            return getOneCategorieMV(categorie);
        }
        return null;
    }

    @Override
    public Categorie addRestaurantToCategorie(Long restaurantId) {
        return null;
    }

    @Override
    public Categorie addCategorie(Categorie categorie) {
        return null;
    }

    @Override
    public boolean deleteCategorie(Long categorieId) {
        return false;
    }

    @Override
    public Categorie updateCategorie(Long categorieId, Categorie categorie) {
        return null;
    }
}
