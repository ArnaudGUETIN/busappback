package com.bonapp.app.service;


import com.bonapp.app.entities.AppRole;
import com.bonapp.app.entities.AppUser;

public interface AccountService {

     AppUser CreateUser(AppUser user);

     AppUser CreateUser(String email,String roleName);

     AppRole CreateRole(AppRole role);

     void addRoleToUser(String username, String roleName);

     AppUser findUserByUsername(String username);

     AppUser updateUser(String Username);

     String generatePassword();
}
