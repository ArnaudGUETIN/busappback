package com.bonapp.app.service;

import com.bonapp.app.entities.Livraison;

import java.util.List;

public interface LivraisonService {
    Livraison addLivraison(Livraison livraison);
    List<Livraison> getAllLivraisons();
    Livraison getOneLivraison(Livraison livraison);
    Livraison getOneLivraisonById(Long livraisonId);

    boolean deleteLivraison(Long livraison);
    Livraison updateCommande(Long livraisonId,Livraison livraison);
}
