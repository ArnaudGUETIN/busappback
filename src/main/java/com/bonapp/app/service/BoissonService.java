package com.bonapp.app.service;

import com.bonapp.app.entities.Boisson;
import com.bonapp.app.modelView.BoissonMV;

import java.util.List;
import java.util.Set;

public interface BoissonService {

    List<BoissonMV> getAllBoissonsMvs();
    Set<BoissonMV> getPlatsOfCategorie(Long categorieId);
    Set<BoissonMV> getBoissonsOfCategorieMV(Long categorieId);
    BoissonMV getOneBoissonMV(Boisson boisson);
    BoissonMV getOneBoissonMVById(Long boissonId);
    List<BoissonMV> getBoissonMvs(List<Boisson> boissons);
    Boisson addBoisson(Boisson boisson);
    boolean deleteBoisson(Long boissonId);
    Boisson updateBoisson(Long boissonId,Boisson boisson);
}
