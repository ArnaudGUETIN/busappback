package com.bonapp.app.service;

import com.bonapp.app.entities.Restaurant;
import com.bonapp.app.modelView.RestaurantMV;

import java.util.Date;
import java.util.List;

public interface RestaurantService {

    List<Restaurant> getAllRestaurants();
    List<RestaurantMV> getAllRestaurantsMV();
    List<RestaurantMV> getAllRestaurantsMV(List<Restaurant> restaurants);
    List<RestaurantMV> getDerniersRestaurantsMV(Long clientId);
    List<RestaurantMV> getNouveauxRestaurantsMV(Date date);
    Restaurant addRestaurant(RestaurantMV restaurantMV);
    RestaurantMV getOneRestaurantMV(Restaurant restaurant);
    RestaurantMV getOneRestaurantMVById(Long restaurantId);
    Restaurant getOneRestaurantById(Long restaurantId);
    Restaurant addRestaurant(Restaurant restaurant);
    boolean deleteRestaurant(Long restaurantId);
    Restaurant updateRestaurant(Long restaurantId,Restaurant restaurant);
}
