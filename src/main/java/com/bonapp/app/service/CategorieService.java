package com.bonapp.app.service;

import com.bonapp.app.entities.Categorie;
import com.bonapp.app.modelView.CategorieMV;

import java.util.List;
import java.util.Set;

public interface CategorieService {

    Set<Categorie> getAllCategories();
    Set<CategorieMV> getAllCategoriesMV();
    Set<CategorieMV> getAllCategoriesMV(List<Categorie> categories);
    CategorieMV getOneCategorieMV(Categorie categorie);
    CategorieMV getOneCategorieMVById(Long categorieId);
    Categorie addRestaurantToCategorie(Long restaurantId);
    Categorie addCategorie(Categorie categorie);
    boolean deleteCategorie(Long categorieId);
    Categorie updateCategorie(Long categorieId,Categorie categorie);
}
