package com.bonapp.app.service;

import com.bonapp.app.entities.LigneCommande;
import com.bonapp.app.modelView.LigneCommandeMV;

import java.util.List;

public interface LigneCommandeService {

    List<LigneCommande> getAllLigneCommandes();
    List<LigneCommandeMV> getAllLigneCommandesMV();
    List<LigneCommandeMV> getAllLigneCommandesMV(List<LigneCommande> ligneCommandes);
    LigneCommandeMV getOneLigneCommandeMV(LigneCommande ligneCommande);
    LigneCommandeMV getOneLigneCommandeMVById(Long ligneCommandeId);
    LigneCommande addLigneCommande(LigneCommandeMV ligneCommandeMV);
    boolean deleteLigneCommande(Long ligneCommandeId);
    LigneCommande updateLigneCommande(Long ligneCommandeId,LigneCommande ligneCommande);
    LigneCommande addLigneCommandeToCommande(Long ligneCommandeId,Long commandId);
}
