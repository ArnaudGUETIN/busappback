package com.bonapp.app.service;

import com.bonapp.app.dao.CommandeRepository;
import com.bonapp.app.dao.LigneCommandeRepository;
import com.bonapp.app.entities.Commande;
import com.bonapp.app.entities.LigneCommande;
import com.bonapp.app.modelView.LigneCommandeMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class LigneCommandeServiceImpl implements LigneCommandeService {

    @Autowired
    private LigneCommandeRepository ligneCommandeRepository;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private PlatService platService;
    @Override
    public List<LigneCommande> getAllLigneCommandes() {
        return ligneCommandeRepository.findAll();
    }

    @Override
    public List<LigneCommandeMV> getAllLigneCommandesMV() {
        return getAllLigneCommandesMV(getAllLigneCommandes());
    }

    @Override
    public List<LigneCommandeMV> getAllLigneCommandesMV(List<LigneCommande> ligneCommandes) {
        List<LigneCommandeMV> toReturn = new ArrayList<>();
        for(LigneCommande ligneCommande:ligneCommandes){
            LigneCommandeMV ligneCommandeMV = new LigneCommandeMV(ligneCommande);
            ligneCommandeMV.setPlatMV(platService.getOnePlatMV(ligneCommande.getPlat()));
            toReturn.add(ligneCommandeMV);
        }
        return toReturn;
    }

    @Override
    public LigneCommandeMV getOneLigneCommandeMV(LigneCommande ligneCommande) {
        return new LigneCommandeMV(ligneCommande);
    }

    @Override
    public LigneCommandeMV getOneLigneCommandeMVById(Long ligneCommandeId) {
        LigneCommande ligneCommande = ligneCommandeRepository.getOne(ligneCommandeId);
        if(ligneCommande!=null){
            return getOneLigneCommandeMV(ligneCommande);
        }
        return null;
    }

    @Override
    public LigneCommande addLigneCommandeToCommande(Long ligneCommandeId, Long commandId) {
        LigneCommande ligneCommande = ligneCommandeRepository.getOne(ligneCommandeId);
        Commande commande = commandeRepository.getOne(commandId);
        if(ligneCommande!=null && commande!=null){
            ligneCommande.setCommande(commande);
        }
        return ligneCommandeRepository.save(ligneCommande);
    }

    @Override
    public LigneCommande addLigneCommande(LigneCommandeMV ligneCommandeMV) {
        LigneCommande ligneCommande = new LigneCommande();
        ligneCommande.setDesignation(ligneCommandeMV.getDesignation());
        ligneCommande.setOrdre(ligneCommandeMV.getOrdre());
        ligneCommande.setPrixUnitaire(ligneCommandeMV.getPrixUnitaire());
        ligneCommande.setQuantite(ligneCommandeMV.getQuantite());
        ligneCommande.setPlat(platService.getOnePlatById(ligneCommandeMV.getPlatMV().getPlatId()));

        return ligneCommandeRepository.save(ligneCommande);
    }

    @Override
    public boolean deleteLigneCommande(Long ligneCommandeId) {
        return false;
    }

    @Override
    public LigneCommande updateLigneCommande(Long ligneCommandeId, LigneCommande ligneCommande) {
        return null;
    }
}
