package com.bonapp.app.service;


import com.bonapp.app.dao.RoleRepository;
import com.bonapp.app.dao.UserRepository;
import com.bonapp.app.entities.AppRole;
import com.bonapp.app.entities.AppUser;
import com.bonapp.app.mail.MailConstructor;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    public static final String ERROR_CODE = "ERROR";
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MailConstructor mailConstructor;


//    @Autowired
//    private JavaMailSender mailSender;

    @Override
    public AppUser CreateUser(AppUser user) {
        // TODO Auto-generated method stub
        String password = user.getPassword();
        String HPW = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(HPW);
        user.setDateCreation(new Date());
        user = userRepository.save(user);

        return user;
    }

    @Override
    public AppUser CreateUser(String email,String roleName) {
        AppUser appUser = new AppUser();
        appUser.setUsername(email);
        String password = generatePassword();
        appUser.setPassword(password);
        appUser.setDateCreation(new Date());
        appUser = CreateUser(appUser);
        AppRole appRole =roleRepository.findByRoleName(roleName);
        if(appRole==null){
            appRole = new AppRole();
            appRole.setRoleName(roleName);
            CreateRole(appRole);
        }

        addRoleToUser(appUser.getUsername(),appRole.getRoleName());
        mailConstructor.constructNewUserEmail(appUser.getUsername(),password);
        return userRepository.save(appUser);
    }

    @Override
    public AppRole CreateRole(AppRole role) {
        // TODO Auto-generated method stub
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        // TODO Auto-generated method stub
        AppRole role = roleRepository.findByRoleName(roleName);
        AppUser user = userRepository.findByUsername(username);
        if(user.getRoles()==null){
            user.setRoles(new ArrayList<>());
        }
        user.getRoles().add(role);
    }

    @Override
    public AppUser findUserByUsername(String username) {
        // TODO Auto-generated method stub
        return userRepository.findByUsername(username);
    }

    @Override
    public AppUser updateUser(String username) {
        AppUser appUser = findUserByUsername(username);
        appUser.setDateDerniereConnexion(new Date());
        appUser.setId(appUser.getId());
        return userRepository.save(appUser);
    }

    @Override
    public String generatePassword() {
        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(2);

        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(2);

        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(2);

        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }

            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);

        String password = gen.generatePassword(10, splCharRule, lowerCaseRule,
                upperCaseRule, digitRule);
        return password;
    }

}
