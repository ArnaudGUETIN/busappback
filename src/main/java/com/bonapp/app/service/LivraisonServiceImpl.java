package com.bonapp.app.service;

import com.bonapp.app.dao.LivraisonRepository;
import com.bonapp.app.entities.Livraison;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LivraisonServiceImpl implements LivraisonService{
    @Autowired
    private LivraisonRepository livraisonRepository;

    @Override
    public Livraison addLivraison(Livraison livraison) {
        return livraisonRepository.save(livraison);
    }

    @Override
    public List<Livraison> getAllLivraisons() {
        return null;
    }

    @Override
    public Livraison getOneLivraison(Livraison livraison) {
        return null;
    }

    @Override
    public Livraison getOneLivraisonById(Long livraisonId) {
        return null;
    }

    @Override
    public boolean deleteLivraison(Long livraison) {
        return false;
    }

    @Override
    public Livraison updateCommande(Long livraisonId, Livraison livraison) {
        return null;
    }
}
