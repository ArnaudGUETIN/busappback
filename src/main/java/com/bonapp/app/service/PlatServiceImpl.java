package com.bonapp.app.service;

import com.bonapp.app.dao.CategorieRepository;
import com.bonapp.app.dao.PlatRepository;
import com.bonapp.app.entities.Categorie;
import com.bonapp.app.entities.Plat;
import com.bonapp.app.modelView.PlatMV;
import com.bonapp.app.modelView.PlatPopulaireMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PlatServiceImpl implements PlatService {

    @Autowired
    private PlatRepository platRepository;
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private CategorieService categorieService;

    @Override
    public Set<Plat> getPlatsOfCategorie(Long categorieId) {
        Categorie categorie = categorieRepository.getOne(categorieId);
        Set<Plat> platList = new HashSet<>();
        if(categorie!=null){
            platList.addAll(categorie.getPlats());
        }
        return platList;
    }

    @Override
    public Set<PlatPopulaireMV> getPlatsPopulaires() {
        List<Plat> platList = platRepository.findAll();
        Set<PlatPopulaireMV> platPopulaireMVS = new HashSet<>();
        for(Plat plat : platList.subList(0,9)){
            platPopulaireMVS.add(new PlatPopulaireMV(plat));
        }
        return platPopulaireMVS;
    }

    @Override
    public PlatMV getOnePlatMV(Plat plat) {
        return new PlatMV(plat);
    }

    @Override
    public Plat getOnePlatById(Long platId) {
        return platRepository.getOne(platId);
    }

    @Override
    public PlatMV getOnePlatMVById(Long platId) {
        Plat plat = platRepository.getOne(platId);
        if(plat!=null){
            return getOnePlatMV(plat);
        }
        return null;
    }

    @Override
    public Plat addPlat(Plat plat) {
        return null;
    }

    @Override
    public boolean deletePlat(Long platId) {
        return false;
    }

    @Override
    public Plat updatePlat(Long platId, Plat commande) {
        return null;
    }

    @Override
    public Set<PlatMV> getPlatsOfCategorieMV(Long categorieId) {
        Categorie categorie = categorieRepository.getOne(categorieId);
        Set<PlatPopulaireMV> platMVHashSet = new HashSet<>();
        if(categorie!=null){
            for(Plat plat:categorie.getPlats()){
                platMVHashSet.add(new PlatPopulaireMV(plat.getPlatId(),plat.getDesignation(),plat.getTempsPreparation().toString(),plat.getPhoto()));
            }
        }
        return null;
    }

    @Override
    public List<PlatMV> getAllPlatsMvs() {
        List<Plat> platList = platRepository.findAll();
        List<PlatMV> platMVList = new ArrayList<>();
        for(Plat plat:platList){
            PlatMV platMV = new PlatMV(plat);
            platMVList.add(platMV);
        }

//        platMVList.get(3).setPlatsPopulaires(new ArrayList<>(getPlatsPopulaires()));
//        platMVList.get(7).setCategorieMVS(new ArrayList<>(categorieService.getAllCategoriesMV()));

        return platMVList;
    }

    @Override
    public List<PlatMV> getPlatMvs(List<Plat> plats) {
        Set<PlatMV> toReturn = new HashSet<>();
        for(Plat plat:plats){
            PlatMV platMV = getOnePlatMV(plat);
            toReturn.add(platMV);
        }
        return new ArrayList<>(toReturn);
    }
}
