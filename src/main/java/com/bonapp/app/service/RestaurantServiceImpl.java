package com.bonapp.app.service;

import com.bonapp.app.dao.CommandeRepository;
import com.bonapp.app.dao.RestaurantRepository;
import com.bonapp.app.dao.UserRepository;
import com.bonapp.app.entities.Adresse;
import com.bonapp.app.entities.AppUser;
import com.bonapp.app.entities.Commande;
import com.bonapp.app.entities.Restaurant;
import com.bonapp.app.modelView.RestaurantMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RestaurantServiceImpl implements RestaurantService{

    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private UserRepository userRepository;
    @Override
    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.findAll();
    }

    @Override
    public List<RestaurantMV> getNouveauxRestaurantsMV(Date date) {
        return getAllRestaurantsMV(restaurantRepository.findAllByDateCreationAfter(date));
    }

    @Override
    public List<RestaurantMV> getDerniersRestaurantsMV(Long clientId) {
        List<Restaurant> toReturn = new ArrayList<>();
        List<Commande> commandeList =commandeRepository.findAllByClientClientId(clientId);
        Set<Long> restaurantIds = new HashSet<>();
        for(Commande commande : commandeList){
            if(!restaurantIds.contains(commande.getRestaurant().getRestaurantId())){
                toReturn.add(commande.getRestaurant());
                restaurantIds.add(commande.getRestaurant().getRestaurantId());
            }


        }
        return getAllRestaurantsMV(toReturn);
    }

    @Override
    public List<RestaurantMV> getAllRestaurantsMV() {
        return getAllRestaurantsMV(getAllRestaurants());
    }

    @Override
    public List<RestaurantMV> getAllRestaurantsMV(List<Restaurant> restaurants) {
        List<RestaurantMV> toReturn = new ArrayList<>();
        for(Restaurant restaurant:restaurants){
            RestaurantMV restaurantMV = new RestaurantMV(restaurant);
            toReturn.add(restaurantMV);
        }
        return toReturn;
    }

    @Override
    public RestaurantMV getOneRestaurantMV(Restaurant restaurant) {
        return new RestaurantMV(restaurant);
    }

    @Override
    public RestaurantMV getOneRestaurantMVById(Long restaurantId) {
        Restaurant restaurant = getOneRestaurantById(restaurantId);
        if(restaurant!=null){
            return getOneRestaurantMV(restaurant);
        }
        return null;
    }

    @Override
    public Restaurant getOneRestaurantById(Long restaurantId) {
        return restaurantRepository.getOne(restaurantId);
    }

    @Override
    public Restaurant addRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    @Override
    public boolean deleteRestaurant(Long restaurantId) {
        return false;
    }

    @Override
    public Restaurant updateRestaurant(Long restaurantId, Restaurant restaurant) {
        return null;
    }

    @Override
    public Restaurant addRestaurant(RestaurantMV restaurantMV) {
        Restaurant restaurant = new Restaurant();
        restaurant.setTelephone(restaurantMV.getTelephone());
        restaurant.setPhoto(restaurantMV.getPhoto());
        restaurant.setInstruction(restaurantMV.getInstruction());
//        restaurant.setHeureFermeture(Time.valueOf(restaurantMV.getHeureFermeture()));
//        restaurant.setHeureOuverture(Time.valueOf(restaurantMV.getHeureOuverture()));
        restaurant.setNom(restaurantMV.getNom());
        restaurant.setEmail(restaurantMV.getEmail());
        restaurant.setDescription(restaurantMV.getDescription());
        restaurant.setAdresse(new Adresse(restaurantMV.getCity(),restaurantMV.getCountry(),restaurantMV.getStreet(),restaurantMV.getStreetNumber(),restaurantMV.getPostalCode()));
        restaurant.setDateCreation(new Date());
        restaurant.setIcon(restaurantMV.getIcon());
        restaurant.setTempsAttente(restaurantMV.getTempsAttente());
        restaurant =addRestaurant(restaurant);
        AppUser appUser =accountService.CreateUser(restaurant.getEmail(),AppUser.USER_RESTAURANT);
        appUser.setRestaurant(restaurant);
        userRepository.save(appUser);
        restaurant.setAppUser(appUser);
        return restaurantRepository.save(restaurant);
    }
}
