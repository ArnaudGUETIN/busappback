package com.bonapp.app.service;

import com.bonapp.app.entities.Plat;
import com.bonapp.app.modelView.PlatMV;
import com.bonapp.app.modelView.PlatPopulaireMV;

import java.util.List;
import java.util.Set;

public interface PlatService {

    List<PlatMV> getAllPlatsMvs();
    Set<Plat> getPlatsOfCategorie(Long categorieId);
    Set<PlatMV> getPlatsOfCategorieMV(Long categorieId);
    Set<PlatPopulaireMV> getPlatsPopulaires();
    PlatMV getOnePlatMV(Plat plat);
    PlatMV getOnePlatMVById(Long platId);
    Plat getOnePlatById(Long platId);
    List<PlatMV> getPlatMvs(List<Plat> plats);
    Plat addPlat(Plat plat);
    boolean deletePlat(Long platId);
    Plat updatePlat(Long platId,Plat plat);

}
