package com.bonapp.app.service;

import com.bonapp.app.dao.ClientRepository;
import com.bonapp.app.dao.CommandeRepository;
import com.bonapp.app.dao.MenuRepository;
import com.bonapp.app.entities.Commande;
import com.bonapp.app.entities.Menu;
import com.bonapp.app.modelView.MenuListMV;
import com.bonapp.app.modelView.MenuMV;
import com.bonapp.app.modelView.PlatMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private PlatService platService;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private BoissonService boissonService;

    @Override
    public List<Menu> getAllMenus() {
        return menuRepository.findMenusByPlatsIsNotNull();
    }

    @Override
    public List<MenuMV> getAllMenusMV() {
        return getAllMenusMV(getAllMenus());
    }

    @Override
    public List<MenuMV> getAllMenusMV(List<Menu> menus) {
        Set<MenuMV> toReturn = new HashSet<>();
        for(Menu menu:menus){
            MenuMV menuMV = getOneMenuMV(menu);
            menuMV.setComposition(generateComposition(platService.getPlatMvs(menu.getPlats())));
            menuMV.setPlatMVList(platService.getPlatMvs(menu.getPlats()));
            menuMV.setBoissonMVS(boissonService.getBoissonMvs(menu.getBoissons()));
            toReturn.add(menuMV);

        }
        return new ArrayList<>(toReturn);
    }

    @Override
    public MenuMV getOneMenuMV(Menu menu) {
        return new MenuMV(menu);
    }

    @Override
    public MenuMV getOneMenuMVById(Long menuId) {
        Menu menu = menuRepository.getOne(menuId);
        if(menu!=null){
            return getOneMenuMV(menu);
        }
        return null;
    }

    @Override
    public List<MenuMV> getPreviousMenus(Long clientId) {
        List<Commande> commandeList = commandeRepository.findAllByClientClientId(clientId);
        return getAllMenusMV(getCommandesMenus(commandeList));
    }

    @Override
    public List<Menu> getCommandesMenus(List<Commande> commandes) {
        List<Menu> toReturn = new ArrayList<>();
        for(Commande commande:commandes){
            toReturn.addAll(commande.getMenus());
        }
        return toReturn;
    }

    @Override
    public String generateComposition(List<PlatMV> platMVS) {
        String toReturn = "";
        StringBuilder stringBuilder = new StringBuilder(toReturn);
        for(PlatMV platMV:platMVS){
            if(platMV!=null){

                stringBuilder.append("-"+platMV.getDesignation());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public List<MenuMV> sortMenuMvByCategorieName(List<MenuMV> menuMVS) {
        menuMVS.sort((o1, o2) -> o1.getCategorieName().compareTo(o2.getCategorieName()));
       return menuMVS;
    }

    @Override
    public List<MenuListMV> getMenuListMV(List<MenuMV> menuMVS,Long restaurantId) {
        menuMVS = sortMenuMvByCategorieName(menuMVS);
        Map<String,List<MenuMV>> stringListHashMap = new HashMap<>();
        List<MenuListMV> toReturn = new ArrayList<>();

        for(MenuMV menuMV:menuMVS){
            menuMV.setRestaurantId(restaurantId);
            if(menuMV.getCategorieName()!=null && stringListHashMap.get(menuMV.getCategorieName())!=null){
                List<MenuMV> mvList = new ArrayList<>(stringListHashMap.get(menuMV.getCategorieName()));
                mvList.add(menuMV);
                stringListHashMap.put(menuMV.getCategorieName(),mvList);
            }else {
                stringListHashMap.put(menuMV.getCategorieName(),Arrays.asList(menuMV));
            }
        }

        stringListHashMap.forEach((s,m)->{
            MenuListMV menuListMV =  new MenuListMV(s,m);
            toReturn.add(menuListMV);


        });

        return toReturn;
    }

    @Override
    public List<MenuMV> getRestaurantMenus(Long restaurantId) {
        List<Menu> menus = menuRepository.findMenusByRestaurantRestaurantId(restaurantId);
        //getMenuListMV(getAllMenusMV(menus));
        return getAllMenusMV(menus);
    }

    @Override
    public Menu addMenu(Menu menu) {
        return null;
    }

    @Override
    public boolean deleteMenu(Long menuId) {
        return false;
    }

    @Override
    public Menu updateMenu(Long menuId, Menu menu) {
        return null;
    }
}
