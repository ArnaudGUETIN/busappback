package com.bonapp.app.service;

import com.bonapp.app.dao.ClientRepository;
import com.bonapp.app.dao.UserRepository;
import com.bonapp.app.entities.AppUser;
import com.bonapp.app.entities.Client;
import com.bonapp.app.modelView.ClientMV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public List<ClientMV> getAllClientsMV() {
        return getAllClientsMV(getAllClients());
    }

    @Override
    public List<ClientMV> getAllClientsMV(List<Client> clients) {
        List<ClientMV> toReturn = new ArrayList<>();
        for (Client client:clients){
            ClientMV clientMV = new ClientMV(client);
            toReturn.add(clientMV);
        }
        return toReturn;
    }

    @Override
    public Client addClient(ClientMV clientMV) {
        Client client = new Client();
        client.setEmail(clientMV.getEmail());
        client.setNom(clientMV.getNom());
        client.setPrenom(clientMV.getPrenom());
        client.setAdresse(clientMV.getAdresse());
        client =addClient(client);
        AppUser appUser =accountService.CreateUser(client.getEmail(),AppUser.USER_CLIENT);
        appUser.setClient(client);
        userRepository.save(appUser);
        client.setAppUser(appUser);
        return clientRepository.save(client);
    }

    @Override
    public ClientMV getOneClientMV(Client client) {
        return new ClientMV(client);
    }

    @Override
    public ClientMV getOneClientMVById(Long clientId) {
        Client client = getOneClientById(clientId);
        if(client!=null){
            return getOneClientMV(client);
        }
        return null;
    }

    @Override
    public Client getOneClientById(Long clientId) {
        return clientRepository.getOne(clientId);
    }

    @Override
    public Client addClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public boolean deleteClient(Long ClientId) {
        return false;
    }

    @Override
    public Client updateClient(Long clientId, ClientMV clientMV) {
        Client client = clientRepository.getOne(clientId);
        if(clientMV!=null){
            client.setNom(clientMV.getNom());
            client.setPrenom(clientMV.getPrenom());
            client.setAdresse(clientMV.getAdresse());
            client.setEmail(clientMV.getEmail());
            client.setTelephone(clientMV.getTelephone());

        }
        return clientRepository.save(client);
    }
}
