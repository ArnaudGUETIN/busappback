package com.bonapp.app.service;

import com.bonapp.app.entities.Supplement;
import com.bonapp.app.modelView.SupplementMV;

import java.util.List;
import java.util.Set;

public interface SupplementService {
    List<SupplementMV> getAllSupplementsMvs();
    Set<SupplementMV> getPlatsOfCategorie(Long categorieId);
    Set<SupplementMV> getSupplementsOfCategorieMV(Long categorieId);
    SupplementMV getOneSupplementMV(Supplement supplement);
    SupplementMV getOneSupplementMVById(Long supplementId);
    List<SupplementMV> getSupplementMvs(List<Supplement> supplements);
    Supplement addSupplement(Supplement supplement);
    boolean deleteSupplement(Long supplementId);
    Supplement updateSupplement(Long supplementId,Supplement supplement);
}
