package com.bonapp.app;

import com.bonapp.app.Utils.CategorieCreator;
import com.bonapp.app.Utils.MenuCreator;
import com.bonapp.app.Utils.PlatCreator;
import com.bonapp.app.Utils.RestaurantCreator;
import com.bonapp.app.dao.CategorieRepository;
import com.bonapp.app.dao.MenuRepository;
import com.bonapp.app.dao.PlatRepository;
import com.bonapp.app.dao.RestaurantRepository;
import com.bonapp.app.entities.*;
import com.bonapp.app.service.AccountService;
import com.bonapp.app.service.RestaurantService;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.velocity.VelocityEngineFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class BonappApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BonappApplication.class, args);
	}


	@Autowired
	private PlatRepository platRepository;
	@Autowired
	private RestaurantRepository restaurantRepository;
	@Autowired
	private CategorieRepository categorieRepository;
	@Autowired
	private MenuRepository menuRepository;
	@Autowired
	AccountService accountService;
	@Autowired
	RestaurantService restaurantService;
	@Bean
	public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
		VelocityEngineFactory velocityEngineFactory = new VelocityEngineFactory();
		Properties props = new Properties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

		velocityEngineFactory.setVelocityProperties(props);
		return velocityEngineFactory.createVelocityEngine();
	}
    @Bean
    public BCryptPasswordEncoder getBCPE() {

        return new BCryptPasswordEncoder();
    }
	@Override
	public void run(String... args) throws Exception {

		AppUser appUser = new AppUser();
		appUser.setUsername("guetin.arnaud@gmail.com");
		appUser.setPassword("1234");
		appUser.setDateCreation(new Date());
		AppRole appRole = new AppRole();

		appRole.setRoleName(AppUser.USER_ADMIN);
		//appUser=accountService.CreateUser(appUser);
		//appRole=accountService.CreateRole(appRole);
		//accountService.addRoleToUser(appUser.getUsername(),appRole.getRoleName());

		Restaurant restaurant1 = RestaurantCreator.createRestaurant("Burger King","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant2 = RestaurantCreator.createRestaurant("KFC","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant3 = RestaurantCreator.createRestaurant("Mac Donalds","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant4 = RestaurantCreator.createRestaurant("Pizza Hut","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant5 = RestaurantCreator.createRestaurant("Dominos","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant6 = RestaurantCreator.createRestaurant("Lady Sushi","","",null,null,null,"","","","../../assets/food5.jpg","");
		Restaurant restaurant7 = RestaurantCreator.createRestaurant("O' Tacos","otacos-montp@gmail.com","",null,null,null,"","","","../../assets/food5.jpg","");
		//restaurant7.setAdresse(new Adresse("Montpellier","France","RUE CHAPTAL",3,"34000"));
		//restaurant7 = restaurantRepository.save(restaurant7);
		//restaurantService.addRestaurant(new RestaurantMV(restaurant7));
		Categorie categorie1 = CategorieCreator.createCategorie("Plat chaud",restaurant2);
		Categorie categorie2 = CategorieCreator.createCategorie("Grillades",restaurant2);
		Categorie categorie3 = CategorieCreator.createCategorie("Pizza",restaurant2);
		//Categorie categorie4 = CategorieCreator.createCategorie("Glace",restaurantRepository.getOne(5L));
		//Categorie categorie5 = CategorieCreator.createCategorie("Crêpes",restaurantRepository.getOne(5L));
		//Categorie categorie3 = CategorieCreator.createCategorie("Pizza",restaurant3);
//		categorieRepository.save(categorie1);
//		categorieRepository.save(categorie2);
//		categorieRepository.save(categorie3);
		Plat plat = PlatCreator.createPlat("Babenda","../../assets/food1.jpg", BigDecimal.valueOf(7.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat1 = PlatCreator.createPlat("Riz soumbala","../../assets/food1.jpg", BigDecimal.valueOf(5.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat2 = PlatCreator.createPlat("Gonré","../../assets/food1.jpg", BigDecimal.valueOf(5.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat3 = PlatCreator.createPlat("Soupe de lapin","../../assets/food1.jpg", BigDecimal.valueOf(6.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat4 = PlatCreator.createPlat("Soupe de poisson","../../assets/food1.jpg", BigDecimal.valueOf(3.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat5 = PlatCreator.createPlat("Soupe de mouton","../../assets/food1.jpg", BigDecimal.valueOf(10.99),categorie1, Time.valueOf("00:00:30"));
		Plat plat6 = PlatCreator.createPlat("Tô sauce feuille ","../../assets/food1.jpg", BigDecimal.valueOf(9.5),categorie1, Time.valueOf("00:00:30"));

		Plat plat7 = PlatCreator.createPlat("Viande de lapin","../../assets/food1.jpg", BigDecimal.valueOf(8.3),categorie2, Time.valueOf("00:00:30"));
		Plat plat8 = PlatCreator.createPlat("Viande de phacophère ","../../assets/food1.jpg", BigDecimal.valueOf(5.3),categorie2, Time.valueOf("00:00:30"));
		Plat plat9 = PlatCreator.createPlat("Viande haousa ","../../assets/food1.jpg", BigDecimal.valueOf(3.5),categorie2, Time.valueOf("00:00:30"));
		Plat plat10 = PlatCreator.createPlat("Pourré ","../../assets/food1.jpg", BigDecimal.valueOf(4.6),categorie2, Time.valueOf("00:00:30"));
		Plat plat11 = PlatCreator.createPlat("Foie grillé ","../../assets/food1.jpg", BigDecimal.valueOf(5.5),categorie2, Time.valueOf("00:00:30"));


		Plat plat12 = PlatCreator.createPlat("Pizza 4 fromages ","../../assets/food1.jpg", BigDecimal.valueOf(7.5),categorie3, Time.valueOf("00:00:30"));
		Plat plat13 = PlatCreator.createPlat("Pizza barbecue ","../../assets/food1.jpg", BigDecimal.valueOf(7.00),categorie3, Time.valueOf("00:00:30"));
//		platRepository.save(plat);
//		platRepository.save(plat1);
//		platRepository.save(plat2);
//		platRepository.save(plat3);
//		platRepository.save(plat4);
//		platRepository.save(plat5);
//		platRepository.save(plat6);
//		platRepository.save(plat7);
//		platRepository.save(plat8);
//		platRepository.save(plat9);
//		platRepository.save(plat10);
//		platRepository.save(plat11);
//		platRepository.save(plat12);
//		platRepository.save(plat13);

		Menu menu = MenuCreator.createMenu("Special Etudiant",BigDecimal.valueOf(12.99),new Date(),"");
		Menu menu1 = MenuCreator.createMenu("Big Menu",BigDecimal.valueOf(8.99),new Date(),"");
		Menu menu2 = MenuCreator.createMenu("Menu du jour",BigDecimal.valueOf(15.99),new Date(),"");
		Menu menu3 = MenuCreator.createMenu("Special Kids",BigDecimal.valueOf(11.99),new Date(),"");
		Menu menu4 = MenuCreator.createMenu("Menu XXL",BigDecimal.valueOf(7.99),new Date(),"");
		Menu menu5 = MenuCreator.createMenu("Menu decouverte",BigDecimal.valueOf(4.99),new Date(),"");
		//menuRepository.save(menu);
		//menuRepository.save(menu1);
		//menuRepository.save(menu2);
		//menuRepository.save(menu3);
		//menuRepository.save(menu4);
		//menuRepository.save(menu5);

		List<Plat> platList = platRepository.findAll();
		for(int i = 0; i< platList.size(); i++){
			if(i%3==0){
				platList.get(i).setMenu(menu);
			}
			if(i%3==1){
				platList.get(i).setMenu(menu1);
			}
			if(i%3==2){
				platList.get(i).setMenu(menu2);
			}
		}

		//platRepository.saveAll(platList);


	}
}
