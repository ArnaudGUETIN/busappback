package com.bonapp.app.web;

import com.bonapp.app.modelView.MenuListMV;
import com.bonapp.app.modelView.MenuMV;
import com.bonapp.app.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class MenuRestRessources {

    @Autowired
    private MenuService menuService;


    @RequestMapping(value = "/allMenus",method = RequestMethod.GET)
    public List<MenuMV> getAllPlatMvs(){
        return menuService.getAllMenusMV();
    }

    @RequestMapping(value = "/previousMenus/{clientId}",method = RequestMethod.GET)
    public List<MenuMV> getAllPreviousMenus(@PathVariable("cientId") Long clientId){
        return menuService.getPreviousMenus(clientId);
    }

    @RequestMapping(value = "/menus/{restaurantId}",method = RequestMethod.GET)
    public List<MenuListMV> getRestaurantsMenus(@PathVariable("restaurantId") Long restaurantId){
        List<MenuMV> restaurantMenus = menuService.getRestaurantMenus(restaurantId);
        return menuService.getMenuListMV(restaurantMenus,restaurantId);
    }
    @RequestMapping(value = "/menu/{menuId}",method = RequestMethod.GET)
    public MenuMV getAllOneMenu(@PathVariable("menuId") Long menuId){
        return menuService.getOneMenuMVById(menuId);
    }
}
