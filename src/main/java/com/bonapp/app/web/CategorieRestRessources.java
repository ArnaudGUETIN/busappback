package com.bonapp.app.web;

import com.bonapp.app.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class CategorieRestRessources {

    @Autowired
    private CategorieService categorieService;
}
