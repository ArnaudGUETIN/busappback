package com.bonapp.app.web;

import com.bonapp.app.modelView.RestaurantMV;
import com.bonapp.app.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class RestaurantRestRessources {

    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(value = "/allRestaurants",method = RequestMethod.GET)
    public List<RestaurantMV> getAllRestaurants(){
        return restaurantService.getAllRestaurantsMV();
    }

    @RequestMapping(value = "/allDerniersRestaurants",method = RequestMethod.GET)
    public List<RestaurantMV> getAllDerniersRestaurants(@RequestParam("clientId") Long clientId){
        return restaurantService.getDerniersRestaurantsMV(clientId);
    }

    @RequestMapping(value = "/restaurant/{restaurantId}",method = RequestMethod.GET)
    public RestaurantMV getOneRestaurant(@PathVariable("restaurantId") Long restaurantId){
        return restaurantService.getOneRestaurantMVById(restaurantId);
    }

    @RequestMapping(value = "/allNouveauxRestaurants",method = RequestMethod.GET)
    public List<RestaurantMV>  getAllNouveauxRestaurantMVS(){
        return restaurantService.getNouveauxRestaurantsMV(Date.from(ZonedDateTime.now().minusMonths(1).toInstant()));
    }
}
