package com.bonapp.app.web;

import com.bonapp.app.modelView.PlatMV;
import com.bonapp.app.service.PlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class PlatRestRessources {

    @Autowired
    private PlatService platService;


    @RequestMapping(value = "/allPlats",method = RequestMethod.GET)
    public List<PlatMV> getAllPlatMvs(){
        return platService.getAllPlatsMvs();
    }

    @RequestMapping(value = "/plat/{platId}",method = RequestMethod.GET)
    public PlatMV getOnePlatMv(@PathVariable("platId") Long platId){
        return platService.getOnePlatMVById(platId);
    }
}
