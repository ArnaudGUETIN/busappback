package com.bonapp.app.web;

import com.bonapp.app.modelView.CommandeMV;
import com.bonapp.app.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class CommandeRestRessources {

    @Autowired
    private CommandeService commandeService;

    @RequestMapping(value = "addCommande",method = RequestMethod.POST)
    private CommandeMV addCommande(@RequestBody CommandeMV commandeMV){
        return commandeService.getOneCommandeMV(commandeService.addCommande(commandeMV));
    }


    @RequestMapping(value = "previousCommandes",method = RequestMethod.GET)
    private List<CommandeMV> getPreviousCommandes(@RequestParam("restaurantId") Long  restaurantId, @RequestParam("clientId") Long  clientId){
        return commandeService.getPreviousCommande(restaurantId,clientId);
    }
}
