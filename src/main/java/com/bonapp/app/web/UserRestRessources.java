package com.bonapp.app.web;

import com.bonapp.app.dao.UserRepository;
import com.bonapp.app.modelView.UserMV;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class UserRestRessources {

    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value = "/userDetails",method = RequestMethod.POST)
    private ResponseEntity getUserDetails(@RequestBody String username) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String jsonString = mapper.writeValueAsString(new UserMV(userRepository.findByUsername(username)));
        return new ResponseEntity(jsonString, HttpStatus.OK);
    }
}
