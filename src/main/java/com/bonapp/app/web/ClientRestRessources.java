package com.bonapp.app.web;

import com.bonapp.app.dao.UserRepository;
import com.bonapp.app.entities.AppUser;
import com.bonapp.app.modelView.ClientMV;
import com.bonapp.app.service.ClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/bonapp")
@CrossOrigin(origins = "*")
public class ClientRestRessources {

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "addClient",method = RequestMethod.POST)
    private ClientMV addClient(@RequestBody ClientMV clientMV){
        return clientService.getOneClientMV(clientService.addClient(clientMV));
    }

    @RequestMapping(value = "getClient",method = RequestMethod.POST)
    private ResponseEntity getOneClient(@RequestBody String username) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        AppUser appUser = userRepository.findByUsername(username);
        String jsonString = mapper.writeValueAsString(new ClientMV(appUser.getClient()));
        return new ResponseEntity(jsonString, HttpStatus.OK);
    }

    @RequestMapping(value = "updateClient",method = RequestMethod.PUT)
    private ClientMV updateClient(@RequestParam("clientId") Long clientId,@RequestBody ClientMV clientMV){

        return new ClientMV(clientService.updateClient(clientId,clientMV));
    }
}
