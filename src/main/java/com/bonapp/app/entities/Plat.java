package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;

@Entity
@Table(name = "t_plat")
public class Plat implements Serializable {

    @Id
    @GeneratedValue
    private Long platId;
    private String designation;
    private BigDecimal prix;
    private int statut;
    private String photo;
    @ManyToOne
    private Categorie categorie;
    @ManyToOne
    private Menu menu;
    private Time tempsPreparation;
    private BigDecimal note;

    @ManyToOne
    private LigneCommande ligneCommande;

    public Long getPlatId() {
        return platId;
    }

    public void setPlatId(Long platId) {
        this.platId = platId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public LigneCommande getLigneCommande() {
        return ligneCommande;
    }

    public void setLigneCommande(LigneCommande ligneCommande) {
        this.ligneCommande = ligneCommande;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Time getTempsPreparation() {
        return tempsPreparation;
    }

    public void setTempsPreparation(Time tempsPreparation) {
        this.tempsPreparation = tempsPreparation;
    }

    public BigDecimal getNote() {
        return note;
    }

    public void setNote(BigDecimal note) {
        this.note = note;
    }
}
