package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "t_boisson")
public class Boisson implements Serializable {

    @Id
    @GeneratedValue
    private Long boissonId;
    private String designation;
    private BigDecimal prix;
    private int statut;
    @ManyToOne
    private Menu menu;
    @ManyToOne
    private Categorie categorie;


    public Long getBoissonId() {
        return boissonId;
    }

    public void setBoissonId(Long boissonId) {
        this.boissonId = boissonId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}
