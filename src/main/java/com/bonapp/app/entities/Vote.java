package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "t_vote")
public class Vote implements Serializable {

    @Id
    @GeneratedValue
    private Long voteId;
    @Temporal(TemporalType.DATE)
    private Date dateVote;
    private String commentaire;
    private int note;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Client client;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Restaurant restaurant;

    public Long getVoteId() {
        return voteId;
    }

    public void setVoteId(Long voteId) {
        this.voteId = voteId;
    }

    public Date getDateVote() {
        return dateVote;
    }

    public void setDateVote(Date dateVote) {
        this.dateVote = dateVote;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
