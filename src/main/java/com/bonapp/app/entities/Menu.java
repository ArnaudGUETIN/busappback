package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "t_menu")
public class Menu implements Serializable {
    @Id
    @GeneratedValue
    private Long menuId;
    private String libelle;
    private BigDecimal prix;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMenu;
    @OneToMany(mappedBy = "menu",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Plat> plats;
    @OneToMany(mappedBy = "menu",cascade = CascadeType.ALL)
    private List<Supplement> supplements;
    @OneToMany(mappedBy = "menu",cascade = CascadeType.ALL)
    private List<Boisson> boissons;
    @ManyToMany
    private Set<Commande> commandes;
    private String photo;
    @ManyToOne
    private Restaurant restaurant;
    @ManyToOne
    private Categorie categorie;


    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public Date getDateMenu() {
        return dateMenu;
    }

    public void setDateMenu(Date dateMenu) {
        this.dateMenu = dateMenu;
    }

    public List<Plat> getPlats() {
        return plats;
    }

    public void setPlats(List<Plat> plats) {
        this.plats = plats;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    @Transient
    public String getRestaurantName() {
        if(getRestaurant()!=null){
            return getRestaurant().getNom();
        }
        return null;
    }

    public Set<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(Set<Commande> commandes) {
        this.commandes = commandes;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Transient
    public String getRestaurantTempsAttente() {
        if(getRestaurant()!=null){
            return getRestaurant().getTempsAttente();
        }

        return null;
    }

    @Transient
    public BigDecimal getRestaurantNote() {
        if(getRestaurant()!=null){
            return getRestaurant().getNote();
        }

        return null;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Transient
    public String getNomCategorie(){
        return getCategorie()!=null?getCategorie().getDesignation():"";
    }

    public List<Supplement> getSupplements() {
        return supplements;
    }

    public void setSupplements(List<Supplement> supplements) {
        this.supplements = supplements;
    }

    public List<Boisson> getBoissons() {
        return boissons;
    }

    public void setBoissons(List<Boisson> boissons) {
        this.boissons = boissons;
    }
}
