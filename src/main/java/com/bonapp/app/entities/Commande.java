package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "t_commande")
public class Commande implements Serializable {

    public final static int STATUT_COMMANDE_ENVOYE=0;
    public final static int STATUT_COMMANDE_EN_PREPARATION=1;
    public final static int STATUT_COMMANDE_REMIS_AU_LIVREUR=2;
    public final static int STATUT_COMMANDE_TERMINEE=3;
    public final static int STATUT_COMMANDE_ANNULEE=-4;

    @Id
    @GeneratedValue
    private Long CommandeId;
    private String numeroCommande;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommande;
    private String description;
    private int statut;
    private String adresseLivraison;
    private String instruction;
    @OneToMany(mappedBy = "commande",cascade = CascadeType.ALL,fetch=FetchType.EAGER)
    private Set<LigneCommande> ligneCommandes;
    @ManyToMany(mappedBy = "commandes",cascade = CascadeType.ALL)
    private Set<Menu> menus;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Restaurant restaurant;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAcceptation;
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePriseEncharge;

    public Long getCommandeId() {
        return CommandeId;
    }

    public void setCommandeId(Long commandeId) {
        CommandeId = commandeId;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Set<LigneCommande> getLigneCommandes() {
        return ligneCommandes;
    }

    public void setLigneCommandes(Set<LigneCommande> ligneCommandes) {
        this.ligneCommandes = ligneCommandes;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Date getDateAcceptation() {
        return dateAcceptation;
    }

    public void setDateAcceptation(Date dateAcceptation) {
        this.dateAcceptation = dateAcceptation;
    }

    public Date getDatePriseEncharge() {
        return datePriseEncharge;
    }

    public void setDatePriseEncharge(Date datePriseEncharge) {
        this.datePriseEncharge = datePriseEncharge;
    }
}
