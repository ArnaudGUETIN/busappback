package com.bonapp.app.entities;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class AppUser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String USER_ADMIN = "ADMIN";
    public static final String USER_CLIENT = "CLIENT";
    public static final String USER_RESTAURANT = "RESTAURANT";
    public static final String USER_LIVREUR = "LIVREUR";
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    private Date dateCreation;
    private Date dateDerniereConnexion;
    @OneToOne
    private Client client;
    @OneToOne
    private Livreur livreur;
    @OneToOne
    private Restaurant restaurant;
    @Transient
    private String currentRole;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    private List<AppRole> roles;
    @Transient
    private String userType;


    public AppUser() {
        super();
    }


    public AppUser(String username, String password, Date dateCreation, List<AppRole> roles) {
        super();
        this.username = username;
        this.password = password;
        this.dateCreation = dateCreation;
        this.roles = roles;
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public Date getDateCreation() {
        return dateCreation;
    }


    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public Date getDateDerniereConnexion() {
        return dateDerniereConnexion;
    }


    public void setDateDerniereConnexion(Date dateDerniereConnexion) {
        this.dateDerniereConnexion = dateDerniereConnexion;
    }


    public List<AppRole> getRoles() {
        return roles;
    }


    public void setRoles(List<AppRole> roles) {
        this.roles = roles;
    }


    public String getCurrentRole() {
        return currentRole;
    }


    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Livreur getLivreur() {
        return livreur;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getUserType() {
        String type = "";
        if(getClient()!=null){
            type = USER_CLIENT;
        }else if(getRestaurant()!=null){
            type = USER_RESTAURANT;
        }else if(getLivreur()!=null){
            type = USER_LIVREUR;
        }else {
            type = USER_ADMIN;
        }
        userType = type;
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
