package com.bonapp.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "t_categorie")
public class Categorie implements Serializable {

    @Id
    @GeneratedValue
    private Long categorieId;
    private String designation;
    @ManyToOne
    private Restaurant restaurant;
    @OneToMany(mappedBy = "categorie",cascade = CascadeType.ALL)
    private List<Plat> plats;
    private String photo;
    @OneToMany(mappedBy = "categorie",cascade = CascadeType.ALL)
    private List<Menu> menus;

    @OneToMany(mappedBy = "categorie",cascade = CascadeType.ALL)
    private List<Supplement> supplements;

    @OneToMany(mappedBy = "categorie",cascade = CascadeType.ALL)
    private List<Boisson> boissons;
    public Long getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Long categorieId) {
        this.categorieId = categorieId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public List<Plat> getPlats() {
        return plats;
    }

    public void setPlats(List<Plat> plats) {
        this.plats = plats;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Transient
    public String getNomRestaurant(){
        return getRestaurant()!=null?getRestaurant().getNom():"";
    }


    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<Supplement> getSupplements() {
        return supplements;
    }

    public void setSupplements(List<Supplement> supplements) {
        this.supplements = supplements;
    }

    public List<Boisson> getBoissons() {
        return boissons;
    }

    public void setBoissons(List<Boisson> boissons) {
        this.boissons = boissons;
    }
}
