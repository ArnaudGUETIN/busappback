package com.bonapp.app.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Embeddable;

@Embeddable
public class Adresse {
    @JsonProperty(value= "city")
    private String city;
    @JsonProperty(value= "country")
    private String country;
    @JsonProperty(value= "street")
    private String street;
    @JsonProperty(value= "streetNumber")
    private int streetNumber;
    @JsonProperty(value= "postalCode")
    private String postalCode;

    public Adresse() {
    }

    public Adresse(String city, String country, String street, int streetNumber, String postalCode) {
        this.city = city;
        this.country = country;
        this.street = street;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
