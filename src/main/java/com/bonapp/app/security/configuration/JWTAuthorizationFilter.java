package com.bonapp.app.security.configuration;


import com.bonapp.app.entities.AppUser;
import com.bonapp.app.service.AccountService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

    @Autowired
    private AccountService accountService;
    private AppUser appUser = new AppUser();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Methods", "*");
        response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, "
                + "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization,Access-Control-Allow-Headers");
        response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, "
                + "Access-Control-Allow-Credentials, Authorization");
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            String jwtString = request.getHeader(SecurityConstants.HEADER_STRING);
            //System.out.println(jwtString);
            if (!request.getRequestURL().toString().contains("http://localhost:8080/socket")) {
                if (jwtString == null || !jwtString.startsWith(SecurityConstants.TOKEN_PREFIX)) {
                    //System.out.println("jeton incorrect");
                    filterChain.doFilter(request, response);
                    return;
                }

                Claims claims = Jwts.parser()
                        .setSigningKey(SecurityConstants.SECRET)
                        .parseClaimsJws(jwtString.replaceAll(SecurityConstants.TOKEN_PREFIX, ""))
                        .getBody();

                String username = claims.getSubject();
                @SuppressWarnings("unchecked")
                ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>) claims.get("roles");
                Collection<GrantedAuthority> authorities = new ArrayList<>();

                roles.forEach(r -> {
                    authorities.add(new SimpleGrantedAuthority(r.get("authority")));
                    //System.out.println(r.get("authority"));
                    //System.out.println("ça marche");
                });
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(username, null, authorities);
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } else {
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken("", null, null);
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }


            try {
                filterChain.doFilter(request, response);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ServletException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }

    }

}

