package com.bonapp.app.security.configuration;

public class SecurityConstants {

    public static final String SECRET = "guetin.arnaud@gmail.com";
    public static final long EXPIRATION_TIME = 36_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}

