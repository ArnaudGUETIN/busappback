package com.bonapp.app.dao;

import com.bonapp.app.entities.Supplement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplementRepository extends JpaRepository<Supplement,Long> {
}
