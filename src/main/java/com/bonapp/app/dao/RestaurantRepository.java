package com.bonapp.app.dao;

import com.bonapp.app.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface RestaurantRepository extends JpaRepository<Restaurant,Long> {
    List<Restaurant> findAllByDateCreationAfter(Date date);
}
