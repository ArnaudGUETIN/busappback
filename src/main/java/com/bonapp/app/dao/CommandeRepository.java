package com.bonapp.app.dao;

import com.bonapp.app.entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommandeRepository extends JpaRepository<Commande,Long> {
    List<Commande> findAllByClientClientId(Long clientId);
}
