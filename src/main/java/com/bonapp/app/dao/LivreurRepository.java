package com.bonapp.app.dao;

import com.bonapp.app.entities.Livreur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivreurRepository extends JpaRepository<Livreur,Long> {
}
