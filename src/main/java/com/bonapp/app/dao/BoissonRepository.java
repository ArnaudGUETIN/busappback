package com.bonapp.app.dao;

import com.bonapp.app.entities.Boisson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoissonRepository extends JpaRepository<Boisson,Long> {
}
