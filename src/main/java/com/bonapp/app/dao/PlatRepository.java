package com.bonapp.app.dao;

import com.bonapp.app.entities.Plat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatRepository extends JpaRepository<Plat,Long> {
}
