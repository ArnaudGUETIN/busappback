package com.bonapp.app.modelView;

import com.bonapp.app.entities.Client;

public class ClientMV {

    private Long clientId;
    private String nom;
    private String prenom;
    private String email;
    private String adresse;
    private String longitude;
    private String latitude;
    private String telephone;

    public ClientMV(Client client) {
        this(client.getClientId(),client.getNom(),client.getPrenom(),client.getEmail(),client.getAdresse(),client.getLongitude(),client.getLatitude(),client.getTelephone());
    }
    public ClientMV() {
    }

    public ClientMV(Long clientId, String nom, String prenom, String email, String adresse,String longitude,String latitude,String telephone) {
        this.clientId = clientId;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.adresse = adresse;
        this.longitude = longitude;
        this.latitude = latitude;
        this.telephone = telephone;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
