package com.bonapp.app.modelView;

import com.bonapp.app.entities.Adresse;
import com.bonapp.app.entities.Restaurant;

import java.util.List;

public class RestaurantMV {

    private Long restaurantId;
    private String nom;
    private String email;
    private String telephone;
    private String city;
    private String country;
    private String street;
    private int streetNumber;
    private String postalCode;
    private String description;
    private String instruction;
    private String  photo;
    private String heureOuverture;
    private String heureFermeture;
    private String icon;
    private String tempsAttente;
    private String  note;
    private String commentaire;


    private List<CategorieMV> categorieMVS;

    public RestaurantMV(Restaurant res) {
        this(res.getRestaurantId(),res.getNom(),res.getEmail(),res.getTelephone(),res.getAdresse(),res.getDescription(),res.getInstruction(),res.getCommentaire(),res.getPhoto(),"","","4.5",res.getIcon(),res.getTempsAttente(),null);
    }
    public RestaurantMV() {
    }

    public RestaurantMV(Long restaurantId, String nom, String email, String telephone, Adresse adresse, String description, String instruction,String commentaire, String photo, String heureOuverture, String heureFermeture,String note,String icon,String tempsAttente, List<CategorieMV> categorieMVS) {
        this.restaurantId = restaurantId;
        this.nom = nom;
        this.email = email;
        this.telephone = telephone;
        this.city = adresse.getCity();
        this.country = adresse.getCountry();
        this.street = adresse.getStreet();
        this.streetNumber = adresse.getStreetNumber();
        this.postalCode = adresse.getPostalCode();
        this.description = description;
        this.instruction = instruction;
        this.photo = photo;
        this.heureOuverture = heureOuverture;
        this.heureFermeture = heureFermeture;
        this.icon= icon;
        this.note = note;
        this.tempsAttente =tempsAttente;
        this.categorieMVS = categorieMVS;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHeureOuverture() {
        return heureOuverture;
    }

    public void setHeureOuverture(String heureOuverture) {
        this.heureOuverture = heureOuverture;
    }

    public String getHeureFermeture() {
        return heureFermeture;
    }

    public void setHeureFermeture(String heureFermeture) {
        this.heureFermeture = heureFermeture;
    }

    public List<CategorieMV> getCategorieMVS() {
        return categorieMVS;
    }

    public void setCategorieMVS(List<CategorieMV> categorieMVS) {
        this.categorieMVS = categorieMVS;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTempsAttente() {
        return tempsAttente;
    }

    public void setTempsAttente(String tempsAttente) {
        this.tempsAttente = tempsAttente;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}
