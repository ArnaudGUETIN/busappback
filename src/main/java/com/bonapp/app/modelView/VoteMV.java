package com.bonapp.app.modelView;

import com.bonapp.app.entities.Vote;

public class VoteMV {

    private Long voteId;
    private String dateVote;
    private String commentaire;
    private int note;
    private Long clientId;
    private Long restaurantId;


    public VoteMV(Vote vote) {
        this(vote.getVoteId(),vote.getDateVote().toString(),vote.getCommentaire(),vote.getNote(),vote.getClient().getClientId(),vote.getRestaurant().getRestaurantId());
    }

    public VoteMV(Long voteId, String dateVote, String commentaire, int note,Long clientId,Long restaurantId) {
        this.voteId = voteId;
        this.dateVote = dateVote;
        this.commentaire = commentaire;
        this.note = note;
        this.clientId = clientId;
        this.restaurantId = restaurantId;
    }

    public Long getVoteId() {
        return voteId;
    }

    public void setVoteId(Long voteId) {
        this.voteId = voteId;
    }

    public String getDateVote() {
        return dateVote;
    }

    public void setDateVote(String dateVote) {
        this.dateVote = dateVote;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
