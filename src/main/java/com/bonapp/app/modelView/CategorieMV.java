package com.bonapp.app.modelView;

import com.bonapp.app.entities.Categorie;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategorieMV {

    private Long categorieId;
    @JsonProperty("text1")
    private String designation;
    @JsonProperty("text2")
    private String nomRestaurant;
    @JsonProperty("image")
    private String photo;

    public CategorieMV(Categorie categorie) {
        this(categorie.getCategorieId(),categorie.getDesignation(),categorie.getNomRestaurant(),categorie.getPhoto());
    }

    public CategorieMV() {
    }

    public CategorieMV(Long categorieId, String designation, String nomRestaurant, String photo) {
        this.categorieId = categorieId;
        this.designation = designation;
        this.nomRestaurant = nomRestaurant;
        this.photo = photo;
    }

    public Long getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Long categorieId) {
        this.categorieId = categorieId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getNomRestaurant() {
        return nomRestaurant;
    }

    public void setNomRestaurant(String nomRestaurant) {
        this.nomRestaurant = nomRestaurant;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
