package com.bonapp.app.modelView;

import com.bonapp.app.entities.LigneCommande;

import java.math.BigDecimal;

public class LigneCommandeMV {

    private Long ligneCommandeId;
    private int ordre;
    private String designation;
    private int quantite;
    private BigDecimal prixUnitaire;
    private BigDecimal montantTotal;
    private PlatMV platMV;

    public LigneCommandeMV(LigneCommande ligneCommande) {
        this(ligneCommande.getLigneCommandeId(),ligneCommande.getOrdre(),ligneCommande.getDesignation(),ligneCommande.getQuantite(),ligneCommande.getPrixUnitaire(),null);
    }

    public LigneCommandeMV() {
    }

    public LigneCommandeMV(Long ligneCommandeId, int ordre, String designation, int quantite, BigDecimal prixUnitaire, PlatMV platMV) {
        this.ligneCommandeId = ligneCommandeId;
        this.ordre = ordre;
        this.designation = designation;
        this.quantite = quantite;
        this.prixUnitaire = prixUnitaire;
        this.platMV = platMV;
    }

    public Long getLigneCommandeId() {
        return ligneCommandeId;
    }

    public void setLigneCommandeId(Long ligneCommandeId) {
        this.ligneCommandeId = ligneCommandeId;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public PlatMV getPlatMV() {
        return platMV;
    }

    public void setPlatMV(PlatMV platMV) {
        this.platMV = platMV;
    }

    public BigDecimal getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(BigDecimal montantTotal) {
        this.montantTotal = montantTotal;
    }
}
