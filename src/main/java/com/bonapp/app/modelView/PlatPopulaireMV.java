package com.bonapp.app.modelView;

import com.bonapp.app.entities.Plat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PlatPopulaireMV {

    private Long platId;
    @JsonProperty("text1")
    private String designation;
    @JsonProperty("text2")
    private String description;
    @JsonProperty("image")
    private String photo;

    public PlatPopulaireMV(Plat plat) {
        this(plat.getPlatId(),plat.getDesignation(),plat.getTempsPreparation().toString(),plat.getPhoto());
    }
    public PlatPopulaireMV() {
    }

    public PlatPopulaireMV(Long platId, String designation, String description, String photo) {
        this.platId = platId;
        this.designation = designation;
        this.description = description;
        this.photo = photo;
    }

    public Long getPlatId() {
        return platId;
    }

    public void setPlatId(Long platId) {
        this.platId = platId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
