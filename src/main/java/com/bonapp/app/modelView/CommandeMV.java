package com.bonapp.app.modelView;

import com.bonapp.app.entities.Commande;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommandeMV {

    private Long commandeId;
    private String numeroCommande;
    private String description;
    private int statut;
    private String adresseLivraison;
    private String instruction;
    private String dateCommande;
    private Long clientId;
    private Long restaurantId;
    private Long livreurId;
    private List<Long> menusIds;
    private List<LigneCommandeMV> ligneCommandeMVS;
    private BigDecimal totalCommande;

    public CommandeMV(Commande commande) {
        this(commande.getCommandeId(),commande.getNumeroCommande(),commande.getDescription(),commande.getStatut(),commande.getAdresseLivraison(),commande.getInstruction(),commande.getDateCommande(),null);
    }

    public CommandeMV() {
    }

    public CommandeMV(Long commandeId, String numeroCommande, String description, int statut, String adresseLivraison, String instruction, Date dateCommande, List<LigneCommandeMV> ligneCommandeMVS) {
        this.commandeId = commandeId;
        this.numeroCommande = numeroCommande;
        this.description = description;
        this.statut = statut;
        this.adresseLivraison = adresseLivraison;
        this.instruction = instruction;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.dateCommande = dateCommande!=null?sdf.format(dateCommande):"01-01-2000";
        this.ligneCommandeMVS = ligneCommandeMVS;
    }

    public Long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Long commandeId) {
        this.commandeId = commandeId;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public List<LigneCommandeMV> getLigneCommandeMVS() {
        return ligneCommandeMVS;
    }

    public void setLigneCommandeMVS(List<LigneCommandeMV> ligneCommandeMVS) {
        this.ligneCommandeMVS = ligneCommandeMVS;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Long getLivreurId() {
        return livreurId;
    }

    public void setLivreurId(Long livreurId) {
        this.livreurId = livreurId;
    }

    public List<Long> getMenusIds() {
        return menusIds;
    }

    public void setMenusIds(List<Long> menusIds) {
        this.menusIds = menusIds;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public BigDecimal getTotalCommande() {
        return totalCommande;
    }

    public void setTotalCommande(BigDecimal totalCommande) {
        this.totalCommande = totalCommande;
    }
}
