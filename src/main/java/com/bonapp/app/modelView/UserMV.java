package com.bonapp.app.modelView;

import com.bonapp.app.entities.AppUser;

public class UserMV {

    private String username;
    private String dateCreation;
    private String dateDerniereConnexion;
    private String currentRole;
    private String userType;

    public UserMV(AppUser appUser) {
        this(appUser.getUsername(),appUser.getDateCreation().toString(),null,appUser.getCurrentRole(),appUser.getUserType());
    }
    public UserMV() {
    }

    public UserMV(String username, String dateCreation, String dateDerniereConnexion, String currentRole,String userType) {
        this.username = username;
        this.dateCreation = dateCreation;
        this.dateDerniereConnexion = dateDerniereConnexion;
        this.currentRole = currentRole;
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getDateDerniereConnexion() {
        return dateDerniereConnexion;
    }

    public void setDateDerniereConnexion(String dateDerniereConnexion) {
        this.dateDerniereConnexion = dateDerniereConnexion;
    }
    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
