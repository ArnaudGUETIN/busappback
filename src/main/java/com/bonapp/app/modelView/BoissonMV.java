package com.bonapp.app.modelView;

import com.bonapp.app.entities.Boisson;

import java.math.BigDecimal;

public class BoissonMV {

    private Long boissonId;
    private String designation;
    private BigDecimal prix;
    private int statut;
    private boolean checked;

    public BoissonMV(Long boissonId, String designation, BigDecimal prix, int statut) {
        this.boissonId = boissonId;
        this.designation = designation;
        this.prix = prix;
        this.statut = statut;
        this.checked = false;
    }

    public BoissonMV() {
    }

    public BoissonMV(Boisson boisson) {
        this(boisson.getBoissonId(),boisson.getDesignation(),boisson.getPrix(),boisson.getStatut());
    }

    public Long getBoissonId() {
        return boissonId;
    }

    public void setBoissonId(Long boissonId) {
        this.boissonId = boissonId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
