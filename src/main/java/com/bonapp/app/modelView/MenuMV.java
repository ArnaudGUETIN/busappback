package com.bonapp.app.modelView;

import com.bonapp.app.entities.Menu;

import java.math.BigDecimal;
import java.util.List;

public class MenuMV  {

    private Long menuId;
    //@JsonProperty("name")
    private String libelle;
    //@JsonProperty("price")
    private BigDecimal prix;
    private String dateMenu;
    //@JsonProperty("image")
    private String photo;
    private List<PlatMV> platMVList;
    //@JsonProperty("sellerType")
    private String restaurantName;
    private int quantite=0;
    private boolean checked;
    private String composition;
    private String tempsAttenteRestaurant;
    private BigDecimal noteRestaurant;
    private String categorieName;
    private List<SupplementMV> supplementMVS;
    private List<BoissonMV> boissonMVS;
    private Long restaurantId;

    public MenuMV(Menu menu) {
        this(menu.getMenuId(),menu.getLibelle(),menu.getPrix(),menu.getDateMenu().toString(),menu.getPhoto(),null,menu.getRestaurantName(),menu.getRestaurantTempsAttente(),menu.getRestaurantNote(),menu.getNomCategorie(),null);
    }

    public MenuMV() {
    }

    public MenuMV(Long menuId, String libelle, BigDecimal prix, String dateMenu, String photo, List<PlatMV> platMVList,String restaurantName,String tempsAttenteRestaurant,BigDecimal noteRestaurant,String categorieName,List<BoissonMV> boissonMVS) {
        this.menuId = menuId;
        this.libelle = libelle;
        this.prix = prix;
        this.dateMenu = dateMenu;
        this.photo = photo;
        this.platMVList = platMVList;
        this.restaurantName = restaurantName;
        this.tempsAttenteRestaurant = tempsAttenteRestaurant;
        this.noteRestaurant = noteRestaurant;
        this.categorieName = categorieName;
        this.boissonMVS = boissonMVS;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public String getDateMenu() {
        return dateMenu;
    }

    public void setDateMenu(String dateMenu) {
        this.dateMenu = dateMenu;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<PlatMV> getPlatMVList() {
        return platMVList;
    }

    public void setPlatMVList(List<PlatMV> platMVList) {
        this.platMVList = platMVList;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getTempsAttenteRestaurant() {
        return tempsAttenteRestaurant;
    }

    public void setTempsAttenteRestaurant(String tempsAttenteRestaurant) {
        this.tempsAttenteRestaurant = tempsAttenteRestaurant;
    }

    public BigDecimal getNoteRestaurant() {
        return noteRestaurant;
    }

    public void setNoteRestaurant(BigDecimal noteRestaurant) {
        this.noteRestaurant = noteRestaurant;
    }

    public String getCategorieName() {
        return categorieName;
    }

    public void setCategorieName(String categorieName) {
        this.categorieName = categorieName;
    }

    public List<SupplementMV> getSupplemnts() {
        return supplementMVS;
    }

    public void setSupplemnts(List<SupplementMV> supplemnts) {
        this.supplementMVS = supplemnts;
    }

    public List<BoissonMV> getBoissonMVS() {
        return boissonMVS;
    }

    public void setBoissonMVS(List<BoissonMV> boissonMVS) {
        this.boissonMVS = boissonMVS;
    }

    public List<SupplementMV> getSupplementMVS() {
        return supplementMVS;
    }

    public void setSupplementMVS(List<SupplementMV> supplementMVS) {
        this.supplementMVS = supplementMVS;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
