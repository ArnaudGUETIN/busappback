package com.bonapp.app.modelView;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MenuListMV {

    @JsonProperty("typeMenu")
    private String typeMenu;
    @JsonProperty("menuMVList")
    private List<MenuMV> menuMVList;


    public MenuListMV(String typeMenu, List<MenuMV> menuMVList) {
        this.typeMenu = typeMenu;
        this.menuMVList = menuMVList;
    }

    public String getTypeMenu() {
        return typeMenu;
    }

    public void setTypeMenu(String typeMenu) {
        this.typeMenu = typeMenu;
    }
}
