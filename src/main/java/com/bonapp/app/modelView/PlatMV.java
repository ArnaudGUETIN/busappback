package com.bonapp.app.modelView;

import com.bonapp.app.entities.Plat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PlatMV {
    private Long platId;
    private String photo;
    private String nomRestaurant;
    private String designation;
    private String smallPhoto;
    private String promotion;
    private BigDecimal note;
    private String tempsPreparation;
    private BigDecimal prix;
    private String portion;
    private Long restaurantId;
    private boolean checked;
    private List<PlatPopulaireMV> platsPopulaires = new ArrayList<>();
    private List<CategorieMV> categorieMVS = new ArrayList<>();

    public PlatMV(Plat plat){
        this(plat.getPlatId(),plat.getPhoto(),plat.getCategorie().getNomRestaurant(),plat.getDesignation(),"../../assets/offer.png","",plat.getNote(),plat.getTempsPreparation().toString(),plat.getPrix(),"1",plat.getCategorie().getRestaurant().getRestaurantId());
    }
    public PlatMV() {
    }

    public PlatMV(Long platId, String photo, String nomRestaurant, String designation, String smallPhoto, String promotion, BigDecimal note, String tempsPreparation, BigDecimal prix, String portion, Long restaurantId) {
        this.platId=platId;
        this.photo = photo;
        this.nomRestaurant = nomRestaurant;
        this.designation = designation;
        this.smallPhoto = smallPhoto;
        this.promotion = promotion;
        this.note = note;
        this.tempsPreparation = tempsPreparation;
        this.prix = prix;
        this.portion = portion;
        this.restaurantId = restaurantId;
        this.checked = false;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNomRestaurant() {
        return nomRestaurant;
    }

    public void setNomRestaurant(String nomRestaurant) {
        this.nomRestaurant = nomRestaurant;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSmallPhoto() {
        return smallPhoto;
    }

    public void setSmallPhoto(String smallPhoto) {
        this.smallPhoto = smallPhoto;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public BigDecimal getNote() {
        return note;
    }

    public void setNote(BigDecimal note) {
        this.note = note;
    }

    public String getTempsPreparation() {
        return tempsPreparation;
    }

    public void setTempsPreparation(String tempsPreparation) {
        this.tempsPreparation = tempsPreparation;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public String getPortion() {
        return portion;
    }

    public void setPortion(String portion) {
        this.portion = portion;
    }

    public Long getPlatId() {
        return platId;
    }

    public void setPlatId(Long platId) {
        this.platId = platId;
    }

    public List<PlatPopulaireMV> getPlatsPopulaires() {
        return platsPopulaires;
    }

    public void setPlatsPopulaires(List<PlatPopulaireMV> platsPopulaires) {
        this.platsPopulaires = platsPopulaires;
    }

    public void fillAllList(List<Plat> platsCategories,List<Plat> platsPopulaires){


        for (Plat platPopulaire:platsPopulaires){
            PlatPopulaireMV platPopulaireMV = new PlatPopulaireMV(platPopulaire.getPlatId(),platPopulaire.getDesignation(),platPopulaire.getCategorie().getRestaurant().getNom(),platPopulaire.getPhoto());
            this.platsPopulaires.add(platPopulaireMV);
        }
    }

    public List<CategorieMV> getCategorieMVS() {
        return categorieMVS;
    }

    public void setCategorieMVS(List<CategorieMV> categorieMVS) {
        this.categorieMVS = categorieMVS;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
