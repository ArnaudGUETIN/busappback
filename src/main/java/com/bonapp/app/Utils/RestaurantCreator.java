package com.bonapp.app.Utils;

import com.bonapp.app.entities.Adresse;
import com.bonapp.app.entities.Restaurant;

import java.sql.Time;

public class RestaurantCreator {

    public static  Restaurant createRestaurant(String nom, String email, String description, Adresse adresse, Time heureOuverture, Time heureFermeture, String instruction, String Latitude, String longitude, String photo, String telephone){
        Restaurant restaurant = new Restaurant();
        restaurant.setNom(nom);
        restaurant.setEmail(email);
        restaurant.setDescription(description);
        restaurant.setAdresse(adresse);
        restaurant.setHeureOuverture(heureOuverture);
        restaurant.setHeureFermeture(heureFermeture);
        restaurant.setInstruction(instruction);
        restaurant.setLatitude(Latitude);
        restaurant.setLongitude(longitude);
        restaurant.setPhoto(photo);
        restaurant.setTelephone(telephone);
        return restaurant;
    }
}
