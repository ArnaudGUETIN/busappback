package com.bonapp.app.Utils;

import com.bonapp.app.entities.Categorie;
import com.bonapp.app.entities.Restaurant;

public class CategorieCreator {

    public static Categorie createCategorie(String designation, Restaurant restaurant){
        Categorie categorie = new Categorie();

        categorie.setDesignation(designation);
        categorie.setRestaurant(restaurant);
        return  categorie;
    }
}
