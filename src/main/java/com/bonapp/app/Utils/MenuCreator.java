package com.bonapp.app.Utils;

import com.bonapp.app.entities.Menu;

import java.math.BigDecimal;
import java.util.Date;

public class MenuCreator {

    public static Menu createMenu(String libelle, BigDecimal prix, Date dateMenu, String photo){
        Menu menu =  new Menu();
        menu.setLibelle(libelle);
        menu.setPrix(prix);
        menu.setDateMenu(dateMenu);
        menu.setPhoto(photo);

        return menu;
    }
}
