package com.bonapp.app.Utils;

import com.bonapp.app.entities.Categorie;
import com.bonapp.app.entities.Plat;

import java.math.BigDecimal;
import java.sql.Time;

public class PlatCreator {

    public static Plat createPlat(String designation, String photo, BigDecimal prix, Categorie categorie, Time time){
        Plat plat = new Plat();
        plat.setDesignation(designation);
        plat.setPhoto(photo);
        plat.setPrix(prix);
        plat.setCategorie(categorie);
        plat.setTempsPreparation(time);
        return plat;
    }
}
